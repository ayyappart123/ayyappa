public class Method3 {

      static int plus(int j, int k) {
        return j + k;
    }

     static double plus(double n, double m) {
        return n + m;
    }
     static float plus(float a,float y){

       return a+y;

    }
    public static void main(String[] args) {

        System.out.println("int: " + plus(450,650));
        System.out.println("double: " + plus(789528,125522.32));
        System.out.println("float value "+plus(100,200));
    }
}
