import javax.xml.bind.SchemaOutputResolver;
import java.sql.SQLOutput;

public class Example2 {

    int a;
    int b;

    public void set(int a, int b){
        this.a=a;
        this.b=b;
    }
    public void get(){
        System.out.println("Value of A= "+a);
        System.out.println("Value of b= "+b);
    }

    public static void main(String[] args) {
        Example2 show=new Example2();
        show.set(2,3);
        show.get();
    }
}
